
//$(document).ready(function() {
    function onYouTubeIframeAPIReady() {
        modalVideoPlayer = new YT.Player('modalVideoContainer', {
            width: $('#modalVideoModal').width(),
            height: $('#modalVideoModal').width()/16*9,
            videoId: youtubeID,
            playerVars: {
                color: 'white',
                playlist: ''
            },
           /* events: {
                onReady: initialize
            }*/
        });
        $('#modalVideoModal').on('shown.bs.modal', function (e) {
            modalVideoPlayer.playVideo();
        });
        $('#modalVideoModal').on('hide.bs.modal', function (e) {
            modalVideoPlayer.pauseVideo();
        });
    }     
//});