
$(document).ready(function() {
        
      var prod = $("#production-slider");
      prod.owlCarousel({     
                navText: ['',''],
                center: true,
                loop:true,
                margin: 30,
                nav:true,
                autoplay:true,
                autoplayTimeout:3000,
                autoplayHoverPause:true,
                
                thumbs: true,
                thumbImage: true,
                thumbContainerClass: 'owl-thumbs',
                thumbItemClass: 'owl-thumb-item',
                
                responsive:{
                    0:{
                        items:1
                    },    
                    768:{
                        items:2
                    }
                }
            });
    
});