


$(document).ready(function() {
    
    $(".roll-link").on("click","a", function (event) {
        event.preventDefault();
        var id  = $(this).attr('href'),
            top = $(id).offset().top;
        $('body,html').animate({scrollTop: top}, 900);
    });
    
    
    $('body').scrollspy({ target: '.menu-container' });
    
    $(".menu-container").sticky({ topSpacing: 0 });
    $('.menu-container').on('sticky-start', function() { $('body').scrollspy('refresh');});
    $('.menu-container').on('sticky-end', function() { $('body').scrollspy('refresh'); });
    
   jQuery(function($){
        $("#clientPhoneMobile").mask("+7 (999) 999-99-99");
        $("#clientPhoneMobileMiddle").mask("+7 (999) 999-99-99");
        $("#clientPhoneVideoModal").mask("+7 (999) 999-99-99");
        $("#modalOrderClientPhone").mask("+7 (999) 999-99-99");
     });
     
    $("#headerForm").submit(function() {           
           console.log( $(this).serialize());
            $.ajax({                
                type: 'POST',
                url: '/bitrixRest',      
                data: $(this).serialize(), 
                success: function(answer) {
                    console.log('succes: ' + answer);
                    $('.modal').modal('hide');
                    $('#succesModal').modal('show');
                },
                error:  function(xhr, str){
                    console.log('error');
                }
              });
            return false;
	});
        
    $("#middleForm").submit(function() {           
           console.log( $(this).serialize());
            $.ajax({                
                type: 'POST',
                url: '/bitrixRest',      
                data: $(this).serialize(), 
                success: function(answer) {
                    console.log('succes: ' + answer);
                    $('.modal').modal('hide');
                    $('#succesModal').modal('show');
                },
                error:  function(xhr, str){
                    console.log('error');
                }
              });
            return false;
	});    
      
    $("#modalVideoForm").submit(function() {           
           console.log( $(this).serialize());
            $.ajax({                
                type: 'POST',
                url: '/bitrixRest',      
                data: $(this).serialize(), 
                success: function(answer) {
                    console.log('succes: ' + answer);
                    $('.modal').modal('hide');
                    $('#succesModal').modal('show');
                },
                error:  function(xhr, str){
                    console.log('error');
                }
              });
            return false;
	});
    
    $("#modalOrderForm").submit(function() {           
           console.log( $(this).serialize());
            $.ajax({                
                type: 'POST',
                url: '/bitrixRest',      
                data: $(this).serialize(), 
                success: function(answer) {
                    console.log('succes: ' + answer);
                    $('.modal').modal('hide');
                    $('#succesModal').modal('show');
                },
                error:  function(xhr, str){
                    console.log('error');
                }
              });
            return false;
	});
    
    $('#modalOrder').on('show.bs.modal', function (event) {
        var button = $(event.relatedTarget); 
        var name = button.data('name'); 
        var price = button.data('price');
        var img = button.data('img');
        var modal = $(this);
        if(name != undefined){
            $('#modalGoodName').text(name);
            $('#modalGoodPrice').text(price);
            $( "#modalGoodImg" ).css( "background-image", 'url(' + img + ')' ); 
            $('#modalOrderFormTitle').val('Дрова - '+name);
            $('#modalGoodContainer').show();
        }
      });
      
      $('#modalOrder').on('hidden.bs.modal', function (event) {        
            $('#modalGoodContainer').hide();    
            $('#modalOrderFormTitle').val('Дрова - Заказ из всплывающей формы');
      });
      
     $('#collapseInfo').on('show.bs.collapse', function () {
        $('#collapseInfoBtn').hide();
         $('body').scrollspy('refresh');
      }) 
      $('#collapseInfo').on('shown.bs.collapse', function () {
         $('body').scrollspy('refresh');
      }) 
      $('#collapseInfo').on('hidden.bs.collapse', function () {
        $('#collapseInfoBtn').show();        
         $('body').scrollspy('refresh');
      }) 
      
      ymaps.ready(init);
     

});

var myMap; 

function init () {
    // Создание экземпляра карты и его привязка к контейнеру с
    // заданным id ("map").
    myMap = new ymaps.Map('backMap', {
        // При инициализации карты обязательно нужно указать
        // её центр и коэффициент масштабирования.
        center: [58.56182, 49.6231], // Москва
        zoom: 18,
        controls: ['smallMapDefaultSet'],
        scroll: false,
    }, {
        searchControlProvider: 'yandex#search'
    });
    
    myMap.behaviors.disable('scrollZoom');
    
    myMap.geoObjects.add(new ymaps.Placemark([58.562, 49.621337], {
            balloonContent: 'Дрова Киров',
            iconCaption: 'Дрова Киров'
        }, {
            preset: 'islands#yellowDotIconWithCaption'
        }))

}
