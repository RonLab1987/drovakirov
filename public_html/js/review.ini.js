    
    
$(document).ready(function() {
        
        var re = $("#reviews-slider");        
        re.owlCarousel({     
                loop:true,
                margin: 30,
                autoplay:true,
                autoplayTimeout:3000,
                autoplayHoverPause:true,
                
                responsive:{
                    0:{
                        items:1
                    },    
                    768:{
                        items:3
                    }
                }
            });

        $('#reviewsNext').click(function(){re.trigger('next.owl.carousel');});
        $('#reviewsPrev').click(function(){re.trigger('prev.owl.carousel');});
        
        $(".fancybox").fancybox({
		openEffect	: 'none',
		closeEffect	: 'none'
	});
           
});