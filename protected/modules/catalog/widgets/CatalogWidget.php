<?php

/**
 * Обрамляет изображение в рамку, фиксируя его по центру.
 *
 * @author yupe team <team@yupe.ru>
 * @link http://yupe.ru
 * @copyright 2009-2013 amyLabs && Yupe! team
 * @package yupe.modules.image.widgets
 * @since 0.1
 *
 */
use yupe\widgets\YWidget;
Yii::import('application.modules.catalog.models.*');

class CatalogWidget extends YWidget
{
    public $view = 'CatalogWidgetView';
    public $category = '';
    
    public function run() {        
        $goods = Good::model()->getGoods();
        $this->render($this->view, ['goods' => $goods]);
    }
}
