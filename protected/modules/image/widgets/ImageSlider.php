<?php

/**
 * Обрамляет изображение в рамку, фиксируя его по центру.
 *
 * @author yupe team <team@yupe.ru>
 * @link http://yupe.ru
 * @copyright 2009-2013 amyLabs && Yupe! team
 * @package yupe.modules.image.widgets
 * @since 0.1
 *
 */
use yupe\widgets\YWidget;
Yii::import('application.modules.image.models.*');

class ImageSlider extends YWidget
{
    public $view = 'ProductionSliderView';
    public $category = '';
    
    public function run() {        
        $images = Image::model()->getActiveImages($this->category);
        $this->render($this->view, ['images' => $images]);
    }
}
