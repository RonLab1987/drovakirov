<?php
/**
 * Дефолтный контроллер сайта:
 *
 * @category YupeController
 * @package  yupe.controllers
 * @author   YupeTeam <team@yupe.ru>
 * @license  BSD http://ru.wikipedia.org/wiki/%D0%9B%D0%B8%D1%86%D0%B5%D0%BD%D0%B7%D0%B8%D1%8F_BSD
 * @version  0.5.3 (dev)
 * @link     http://yupe.ru
 *
 **/
namespace application\controllers;

use yupe\components\controllers\FrontController;
use yupe\components\Mail;



class SiteController extends FrontController
{
    /**
     * Отображение главной страницы
     *
     * @return void
     */
    
    const FROM = 'zakaz@drova-kirov.ru';
    const TO = ['vds.krohov@gmail.com', 'ride43@yandex.ru'];
    const BODY = 'Телефон заказчика: ';
    
    
    
    public function actionIndex()
    {  /*
        $from = 'zakaz@'.\Yii::app()->homeUrl;
                $to= ['ronlab@yandex.ru', 'ride43@yandex.ru'];
                $theme = \Yii::app()->getRequest()->getPost('title');
                $body = \Yii::app()->getRequest()->getPost('phone');
                Mail()->send($from, $to, $theme, $body);
     
     */
        /*
         $from = 'zakaz@drova-kirov.app';
                $to= ['ronlab@yandex.ru', 'ride43@yandex.ru'];
                $theme = 'Test mail';
                $body = 'This is my test mail ';
                //Mail()->send($from, $to, $theme, $body);
        
        $mail = new Mail();
        $mail->init();
        $mail->send($from, $to, $theme, $body);
        
       */
        
        $this->render('index');
    }
    
    public function actionBitrixRest(){
        
        // CRM server conection data
                define('CRM_HOST', 'finngroup-24.bitrix24.ru'); // your CRM domain name
                define('CRM_PORT', '443'); // CRM server port
                define('CRM_PATH', '/crm/configs/import/lead.php'); // CRM server REST service path

                // CRM server authorization data
                define('CRM_LOGIN', 'vds.krohov@gmail.com'); // login of a CRM user able to manage leads
                define('CRM_PASSWORD', '13061985qq'); // password of a CRM user
                // OR you can send special authorization hash which is sent by server after first successful connection with login and password
                //define('CRM_AUTH', 'e54ec19f0c5f092ea11145b80f465e1a'); // authorization hash
        
        // POST processing
        if (($phone = \Yii::app()->getRequest()->getPost('phone')) !== null) 
        {           
                 $answer =[];    
        
                $mail = new Mail();
                $mail->init();
                if(!$mail->send(self::FROM, self::TO, \Yii::app()->getRequest()->getPost('title') , self::BODY . \Yii::app()->getRequest()->getPost('phone'))){
                    $answer['mailStatus'] = FALSE;                                        
                }
                else{
                    $answer['mailStatus'] = TRUE;
                }; 
                
                          
            
                $postData = array(
                        'TITLE' => \Yii::app()->getRequest()->getPost('title'),
                        'PHONE_MOBILE' => \Yii::app()->getRequest()->getPost('phone'),                       
                );

                // append authorization data
                if (defined('CRM_AUTH'))
                {
                        $postData['AUTH'] = CRM_AUTH;
                }
                else
                {
                        $postData['LOGIN'] = CRM_LOGIN;
                        $postData['PASSWORD'] = CRM_PASSWORD;
                }

                // open socket to CRM
                $fp = fsockopen("ssl://".CRM_HOST, CRM_PORT, $errno, $errstr, 30);
                if ($fp)
                {
                        // prepare POST data
                        $strPostData = '';
                        foreach ($postData as $key => $value)
                                $strPostData .= ($strPostData == '' ? '' : '&').$key.'='.urlencode($value);

                        // prepare POST headers
                        $str = "POST ".CRM_PATH." HTTP/1.0\r\n";
                        $str .= "Host: ".CRM_HOST."\r\n";
                        $str .= "Content-Type: application/x-www-form-urlencoded\r\n";
                        $str .= "Content-Length: ".strlen($strPostData)."\r\n";
                        $str .= "Connection: close\r\n\r\n";

                        $str .= $strPostData;

                        // send POST to CRM
                        fwrite($fp, $str);

                        // get CRM headers
                        $result = '';
                        while (!feof($fp))
                        {
                                $result .= fgets($fp, 128);
                        }7+
                        fclose($fp);

                        // cut response headers
                        $response = explode("\r\n\r\n", $result);

                        $output = print_r($response[1], 1);
                        
                         $answer['bitrixStatus'] = TRUE; 
                        $answer['response'] = $output;
                        echo json_encode($answer);
                }
                else
                {       
                    $answer['bitrixStatus'] = FALSE; 
                    $answer['response'] = 'Connection Failed! '.$errstr.' ('.$errno.')';
                    echo json_encode($answer);
                }
        }
        else
        {   $answer['mailStatus'] = FALSE;
            $answer['bitrixStatus'] = FALSE; 
            $answer['response'] = 'NO DATA'; 
            echo json_encode($answer);
        }
    }

    /**
     * Отображение для ошибок:
     *
     * @return void
     */
    public function actionError()
    {
        $error = \Yii::app()->errorHandler->error;

        if (empty($error) || !isset($error['code']) || !(isset($error['message']) || isset($error['msg']))) {
            $this->redirect(['index']);
        }

        if (!\Yii::app()->getRequest()->getIsAjaxRequest()) {

            $this->render(
                'error',
                [
                    'error' => $error
                ]
            );
        }
    }
    
    public function filters()
    {
        return array(
            array(
                'COutputCache + index',
                'duration'=> 3600,
            ),
        );
    }
}
