
<?php if(!empty($goods)): ?> 
<?php
  //  Yii::app()->getClientScript()->registerScriptFile('/js/review.ini.js', CClientScript::POS_END);
?>

<section class="price-section" id='priceSection'>
    <div class="container">
        <h2>Цены и условия доставки</h2>
        
        <div class="row goods-area">
            
            <?php   foreach ($goods as $good): ?> 
                    <div class="col-xs-12 col-sm-6 col-md-6 col-lg-3">
                                    <div class='good-card' data-toggle="modal" data-target="#modalOrder" 
                                         data-name='<?= $good->name ?>' 
                                         data-price='<?= Yii::app()->getNumberFormatter()->format("#,##0", $good->price) ?>'
                                         data-img='<?= $good->getImageUrl() ?>'
                                    >
                                        <h3><?= $good->name ?></h3>
                                        <div class="good-card-img-area">
                                            <div class="good-card-img" style="background-image: url('<?= $good->getImageUrl() ?>')">                            
                                            </div>
                                        </div>
                                        <div class="good-card-price-area">
                                            <div class="good-card-price">
                                                <?= Yii::app()->getNumberFormatter()->format("#,##0", $good->price) ?> <i class="fa fa-rub" aria-hidden="true"></i>
                                            </div>
                                            <div class="good-card-bye-area">
                                                <span  class="good-card-bye-btn">Заказать</span>
                                            </div>
                                        </div>
                                    </div>                
                    </div>
            
            <?php endforeach;  ?>        
        </div>
        
        <div class="big-ax-area clearfix">
            <span class="big-ax-i-area">
                <span class="big-ax-i"><i>i</i></span>
            </span>
            <div class="big-ax-info">
                <p>Цена указана с доставкой по г. Кирову а/ми ЗиЛ-130, либо Газ-53.</p>
                <p>Доставка в районы считается по тарифу 16 руб/км в обе стороны.</p>
            </div>
        </div>
    </div>
    
</section>

<?php endif; ?>

