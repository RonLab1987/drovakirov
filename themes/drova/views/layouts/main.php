<!DOCTYPE html>
<html lang="<?= Yii::app()->language; ?>">
<head>
    <?php \yupe\components\TemplateEvent::fire(DrovaThemeEvents::HEAD_START);?>
    
    
  
    
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<title><?= $this->yupe->siteName;?></title>
<meta property="og:title" content="<?= $this->yupe->siteName;?>">
<link rel="icon" href="favicon.ico" type="image/x-icon">
<link rel="shortcut icon" href="favicon.ico" type="image/x-icon">
<link rel="apple-touch-icon" sizes="57x57" href="<?= $this->mainAssets ?>/img/fav/apple-icon-57x57.png">
<link rel="apple-touch-icon" sizes="60x60" href="<?= $this->mainAssets ?>/img/fav/apple-icon-60x60.png">
<link rel="apple-touch-icon" sizes="72x72" href="<?= $this->mainAssets ?>/img/fav/apple-icon-72x72.png">
<link rel="apple-touch-icon" sizes="76x76" href="<?= $this->mainAssets ?>/img/fav/apple-icon-76x76.png">
<link rel="apple-touch-icon" sizes="114x114" href="<?= $this->mainAssets ?>/img/fav/apple-icon-114x114.png">
<link rel="apple-touch-icon" sizes="120x120" href="<?= $this->mainAssets ?>/img/fav/apple-icon-120x120.png">
<link rel="apple-touch-icon" sizes="144x144" href="<?= $this->mainAssets ?>/img/fav/apple-icon-144x144.png">
<link rel="apple-touch-icon" sizes="152x152" href="<?= $this->mainAssets ?>/img/fav/apple-icon-152x152.png">
<link rel="apple-touch-icon" sizes="180x180" href="<?= $this->mainAssets ?>/img/fav/apple-icon-180x180.png">
<link rel="icon" type="image/png" sizes="192x192"  href="<?= $this->mainAssets ?>/img/fav/android-icon-192x192.png">
<link rel="icon" type="image/png" sizes="32x32" href="<?= $this->mainAssets ?>/img/fav/favicon-32x32.png">
<link rel="icon" type="image/png" sizes="96x96" href="<?= $this->mainAssets ?>/img/fav/favicon-96x96.png">
<link rel="icon" type="image/png" sizes="16x16" href="<?= $this->mainAssets ?>/img/fav/favicon-16x16.png">
<link rel="manifest" href="<?= $this->mainAssets ?>/img/fav/manifest.json">
<meta name="msapplication-TileColor" content="#ffffff">
<meta name="msapplication-TileImage" content="<?= $this->mainAssets ?>/img/fav/ms-icon-144x144.png">
<meta name="theme-color" content="#ffffff">

<meta name="description" content="<?= $this->yupe->siteDescription;?>">
<meta property="og:description" content="<?= $this->yupe->siteDescription;?>" />

<meta property="og:image" content="<?= $this->mainAssets ?>img/fav/favicon-96x96.png">
<link rel="image_src" href="<?= $this->mainAssets ?>/img/fav/apple-icon-180x180.png">
<link rel="apple-touch-startup-image" href="<?= $this->mainAssets ?>/img/fav/apple-icon-180x180.png">

   
    
    <meta name="keywords" content="<?= $this->yupe->siteKeyWords;?>" />

    <?php if ($this->canonical): ?>
        <link rel="canonical" href="<?= $this->canonical ?>" />
    <?php endif; ?>
    <?php
    
    Yii::app()->getClientScript()->registerCssFile($this->mainAssets . '/css/roboto.css');    
    Yii::app()->getClientScript()->registerCssFile($this->mainAssets . '/css/owl.css');
    
    Yii::app()->getClientScript()->registerCssFile($this->mainAssets . '/css/main.css');
    
    Yii::app()->getClientScript()->registerCssFile($this->mainAssets . '/css/jquery.fancybox.css');
   /* Yii::app()->getClientScript()->registerCssFile($this->mainAssets . '/css/flags.css');
    Yii::app()->getClientScript()->registerCssFile($this->mainAssets . '/css/yupe.css');
    Yii::app()->getClientScript()->registerScriptFile($this->mainAssets . '/js/blog.js');
    Yii::app()->getClientScript()->registerScriptFile($this->mainAssets . '/js/bootstrap-notify.js');
    Yii::app()->getClientScript()->registerScriptFile($this->mainAssets . '/js/jquery.li-translit.js'); */
   // Yii::app()->getClientScript()->registerCssFile('/css/main.css'); 
    
    Yii::app()->getClientScript()->registerScriptFile('/js/maskedInput.js', CClientScript::POS_END);
    Yii::app()->getClientScript()->registerScriptFile('/js/reaction.js', CClientScript::POS_END);    
    Yii::app()->getClientScript()->registerScriptFile('/js/owl.carousel.min.js', CClientScript::POS_END);
    Yii::app()->getClientScript()->registerScriptFile('/js/owl.carousel2.thumbs.min.js', CClientScript::POS_END);
    Yii::app()->getClientScript()->registerScriptFile('/js/jquery.fancybox.min.js', CClientScript::POS_END);
    Yii::app()->getClientScript()->registerScriptFile('/js/jquery.smooth-scroll.min.js', CClientScript::POS_END);    
    Yii::app()->getClientScript()->registerScriptFile('/js/jquery.sticky.js', CClientScript::POS_END);
    Yii::app()->getClientScript()->registerScriptFile('//api-maps.yandex.ru/2.1/?lang=ru_RU', CClientScript::POS_END);
    
    ?>
    <script type="text/javascript">
        var yupeTokenName = '<?= Yii::app()->getRequest()->csrfTokenName;?>';
        var yupeToken = '<?= Yii::app()->getRequest()->getCsrfToken();?>';
    </script>
    <!--[if IE]>
    <script src="http://html5shiv.googlecode.com/svn/trunk/html5.js"></script>
    <![endif]-->
    <?php /*
    <link rel="stylesheet" href="http://yandex.st/highlightjs/8.2/styles/github.min.css">
    <script src="http://yastatic.net/highlightjs/8.2/highlight.min.js"></script>
    */ ?>
    <?php \yupe\components\TemplateEvent::fire(DrovaThemeEvents::HEAD_END);?>
</head>

<body>

<?php \yupe\components\TemplateEvent::fire(DrovaThemeEvents::BODY_START);?>
     <?= $content; ?>
<?php \yupe\components\TemplateEvent::fire(DrovaThemeEvents::BODY_END);?>
<!-- Yandex.Metrika counter --> <script type="text/javascript"> (function (d, w, c) { (w[c] = w[c] || []).push(function() { try { w.yaCounter36952065 = new Ya.Metrika({ id:36952065, clickmap:true, trackLinks:true, accurateTrackBounce:true, webvisor:true }); } catch(e) { } }); var n = d.getElementsByTagName("script")[0], s = d.createElement("script"), f = function () { n.parentNode.insertBefore(s, n); }; s.type = "text/javascript"; s.async = true; s.src = "https://mc.yandex.ru/metrika/watch.js"; if (w.opera == "[object Opera]") { d.addEventListener("DOMContentLoaded", f, false); } else { f(); } })(document, window, "yandex_metrika_callbacks"); </script> <noscript><div><img src="https://mc.yandex.ru/watch/36952065" style="position:absolute; left:-9999px;" alt="" /></div></noscript> <!-- /Yandex.Metrika counter -->  
</body>
</html>
