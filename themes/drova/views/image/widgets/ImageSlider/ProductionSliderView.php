<?php if(!empty($images)): ?>            
<?php
    Yii::app()->getClientScript()->registerScriptFile('/js/slider.ini.js', CClientScript::POS_END);
 ?>   
<section class="photo-section clearfix" id='ourProdaction'>
    <h2>Наше производство</h2>    
    <div class="production-slider-container"> 
        
        
            <div id='production-slider' class="owl-carousel">
                <?php foreach ($images as $image): ?>
                            <?= CHtml::image(
                                 $image->image->getImageUrl(1200),
                                 $image->image->alt,
                                 ['title' => $image->image->alt]
                             ); ?>                    
                 <?php endforeach; ?>                 
            </div>
     </div>
</section> 
<?php endif; ?>

