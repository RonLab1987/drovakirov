

<?php if(!empty($images)): ?> 
<?php
    Yii::app()->getClientScript()->registerScriptFile('/js/review.ini.js', CClientScript::POS_END);
?>
<section class="reviews-section" id="reviewSection">
    <div class="review-back-img">
        <div class="container">
            <h3>Люди о нас</h3>
            <div class="zig-zag-type1">
                <span></span><span></span><span></span><span></span><span></span>
            </div> 

            <div class="reviews-slider-container"> 
                <div id='reviews-slider' class="owl-carousel">
                    <?php foreach ($images as $image): ?> 
                        <div class="review-blanc-area">
                            <div class="review-blanc-top"></div>
                            <div class="review-blanc-container fancybox" rel="gallery1" href="<?= $image->image->getImageUrl(1000); ?>">
                                <?= CHtml::image(
                                    $image->image->getImageUrl(500),
                                    $image->image->alt,
                                    ['title' => $image->image->alt]
                                ); ?>
                            </div>
                            <div class="review-blanc-bottom"></div>
                        </div>

                     <?php endforeach; ?>  
                </div>

                <div class="reviews-slider-nav">
                    <div class="reviews-slider-nav-prev" id='reviewsPrev'></div>
                    <div class="reviews-slider-nav-next" id='reviewsNext'></div>
                </div> 
            </div>
        </div>
    </div>    

</section>
<?php endif; ?>