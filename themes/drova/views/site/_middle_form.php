<section class="middle-form-section">
    <div class="container">
        <div class="middle-form-content-container">
            <h2>Заказать дрова</h2>
            <div class="middle-form-zig-zag-container">
                <div class="zig-zag-type1">
                    <span></span><span></span><span></span><span></span><span></span>
                </div> 
            </div>
            <div class="clearfix">
            <?= CHtml::beginForm([], 'post', ['class' => 'middle-form','id' => 'middleForm'])?>    
                                <div class='hidden'>
                                    <input type="text" name="title" value="Дрова - Заявка из средней формы" />
                                </div>
                            <div class="form-row">   
                                <div class="input-container">                                
                                    <input type="tel" placeholder="Ваш телефон" name='phone' id='clientPhoneMobileMiddle' required="required">
                                </div> 
                                <div class="input-container">
                                    <div class='submit-container'>
                                        <button id="headerFormSubmit" type="submit">Заказать</button>
                                    </div>
                                </div>
                            </div>                               
            </form> 
            </div>
            <p>Мы перезвоним в течении 15 минут</p>
             </div> 
        </div>
    
</section>