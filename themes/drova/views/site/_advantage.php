<section class="adv-section" id='advSection'>
    <div class="container">
        <h3>Почему заказать лучше у нас?</h3>
        
        <div class="adv-area">
            <div class="adv-row">
                <div class="adv-card" data-toggle="modal" data-target="#modalVideoModal">
                    <div class="adv-icon-area">
                        <img src="<?= $this->mainAssets ?>/img/adv1-icon.png">
                    </div>
                    <div class="adv-text-area">
                        Честная цена. ЗиЛ-130 или ГАЗ-53 с нарощенными бортами от 4600р. с доставкой.
                    </div>
                </div> 
                <div class="adv-card" data-toggle="modal" data-target="#modalVideoModal">
                    <div class="adv-icon-area">
                        <img src="<?= $this->mainAssets ?>/img/adv2-icon.png">
                    </div>
                    <div class="adv-text-area">
                        Доставка в течении 2х часов. Наличие собственного автопарка из 4х автомобилей для развозки позволяет 
                        привезти дрова вовремя.
                    </div>
                </div> 
            </div>
            <div class="adv-row">
                <div class="adv-card" data-toggle="modal" data-target="#modalVideoModal">
                    <div class="adv-icon-area">
                        <img src="<?= $this->mainAssets ?>/img/adv3-icon.png">
                    </div>
                    <div class="adv-text-area">
                        Собственное производство. В наличии более 1000 куб.м. разных пород древесины.
                    </div>
                </div> 
                <div class="adv-card" data-toggle="modal" data-target="#modalVideoModal">
                    <div class="adv-icon-area">
                        <img src="<?= $this->mainAssets ?>/img/adv4-icon.png">
                    </div>
                    <div class="adv-text-area">
                        Более 8 лет, бесперебойно, работаем с населением и отапливаем крупные Кировские организации.
                    </div>
                </div> 
            </div>
        </div>
    </div>    
</section>