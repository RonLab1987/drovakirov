<section class="contact-section" id="contactSection">
    <div class="map-container" id='backMap'>
    <?php /*
        <script type="text/javascript" charset="utf-8" async src="https://api-maps.yandex.ru/services/constructor/1.0/js/?sid=4_RrAJfaxP0FdlqR6GPOFp4NNUl7bWxj&amp;width=100%&amp;height=100%&amp;lang=ru_RU&amp;sourceType=constructor&amp;scroll=false"></script>
    */ ?>
   
    </div>

    <div class="container">
        <div class="col-xs-12 col-sm-8 col-md-5 col-lg-4 pull-right">
            <div class="contact-container">
                <h3>Наши контакты</h3>
                <hr>
                <div class="contact-row">
                    <div class="contact-icon-col">
                        <img src="<?= $this->mainAssets ?>/img/contact-geo.png">
                    </div>
                    <div class="contact-info-col">
                        г. Киров, ул. Потребкооперации 19 (на территории бывшей птицефабрики)
                    </div>
                </div>
                <div class="contact-row">
                    <div class="contact-icon-col">
                        <img src="<?= $this->mainAssets ?>/img/contact-phone.png">
                    </div>
                    <div class="contact-info-col">
                        <a class="contact-phone" href="tel:+78332771051">+7(8332) 77-10-51</a>
                        <a class="contact-phone" href="tel:+78332739544">+7(8332) 73-95-44</a>                        
                        <a class="contact-phone" href="tel:+78332502-902">т./ф. 502-902</a>
                    </div>
                </div>
                <div class="contact-row">
                    <div class="contact-icon-col">
                        <img src="<?= $this->mainAssets ?>/img/contact-mail.png">
                    </div>
                    <div class="contact-info-col">
                        <a class="contact-mail" href="mailto:drovakirov@mail.ru">drovakirov@mail.ru</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>






