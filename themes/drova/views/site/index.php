    <?php  $this->renderPartial('_header');  ?>
    <?php  $this->renderPartial('_menu');  ?>
    <?php  $this->widget('application.modules.catalog.widgets.CatalogWidget'); ?> 
    <?php  $this->renderPartial('_info');   ?>
    <?php  $this->renderPartial('_middle_form');   ?>
    <?php  $this->renderPartial('_advantage');   ?>
    <?php  $this->widget('application.modules.image.widgets.ImageSlider',['category' => 'production']); ?>
    <?php  $this->renderPartial('_video');  ?>
    <?php  $this->renderPartial('_director');  ?>
    <?php  $this->renderPartial('_steps');  ?>
    <?php  $this->widget('application.modules.image.widgets.ImageSlider',['category' => 'reviews','view' => 'ReviewSliderView']); ?>
    <?php  $this->renderPartial('_contact');  ?>
