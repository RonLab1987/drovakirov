<section class="header-section" id='header-section'>
    <div class="container">
        <div class="header-top-panel-area">
            <div class="header-top-panel clearfix">
                <div class="company-logo-area">
                    <div class="company-logo roll-link">
                        <a href="#header-section">
                            <img src="<?= $this->mainAssets ?>/img/logo.png">
                        </a>
                    </div>
                    <div class="company-logo-description">
                        <h1>Производство и продажа дров в г. Кирове и Кировской области</h1>
                    </div>
                </div>
                <div class="company-about-number-area hidden-xs hidden-sm hidden-md">
                    <div class="company-about-number" data-toggle="modal" data-target="#modalVideoModal">
                        <img src="<?= $this->mainAssets ?>/img/tree-icon.png">
                        <span>Более 1000 м<sup>3</sup> на складе</span>
                    </div>
                    <div class="company-about-number" data-toggle="modal" data-target="#modalVideoModal">
                        <img src="<?= $this->mainAssets ?>/img/truck-icon.png">
                        <span>Бесплатная доставка по городу</span>
                    </div>
                    <div class="company-about-number" data-toggle="modal" data-target="#modalVideoModal">
                        <img src="<?= $this->mainAssets ?>/img/calendar-icon.png">
                        <span>Более 8 лет на рынке</span>
                    </div>
                </div>
                <div class="company-contact-area">
                    
                        <a class="company-contact-phone" href="tel:+78332771051">(8332) 77-10-51</a>
                        <a href="" class="company-contact-callback-btn hidden-xs" data-toggle="modal" data-target="#modalOrder">Заказать дрова</a>
                                        
                </div>
            </div>
        </div>
        <div class='row'>
            <div class='header-main-area'>
                <div class="header-message-area">
                    <h2>
                        <?php $this->widget(
                            "application.modules.contentblock.widgets.ContentBlockWidget",
                            array("code" => "utp"));
                        ?>
                    </h2>
                    <p>
                        <?php $this->widget(
                            "application.modules.contentblock.widgets.ContentBlockWidget",
                            array("code" => "utp-addon"));
                        ?>
                    </p>
                </div>
                <div class="header-form-area hidden-xs hidden-sm hidden-md">
                    <div class="header-form-container">
                        <div class="header-form-pre-header">
                            <div class="zig-zag-type1 right">
                                <span></span><span></span><span></span>
                            </div>
                            <div class="header-form-pre-header-img">
                                <img src="<?= $this->mainAssets ?>/img/axes-icon.png">
                            </div>
                            <div class="zig-zag-type1 left">
                                <span></span><span></span><span></span>
                            </div>
                        </div>
                        <div class="header-form-header">
                            <h3>Заказать дрова</h3>
                            <a href="tel:+78332771051">т. 77-10-51</a>
                            <span>или</span>
                        </div>    
                       <?php 
                        echo CHtml::beginForm([], 'post', ['class' => 'header-form','id' => 'headerForm'])?>    
                                <div class='hidden'>
                                    <input type="text" name="title" value="Дрова - Заявка из верхней формы" />
                                </div>
                                <input type="tel" placeholder="Ваш телефон" name='phone' id='clientPhoneMobile' required="required">

                                <div class='submit-container'>
                                    <button id="headerFormSubmit" type="submit">Заказать дрова</button>
                                </div>

                        </form> 
                    </div>

                </div>
            </div>
        </div>
    </div>
</section>

<div class="modal fade" tabindex="-1" role="dialog" aria-labelledby="modalOrder" id='modalOrder'>
                        
                    <div class="modal-dialog " role="document">
                      <div class="modal-content " >
                          <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                          
                          <div class="modal-video-form-container clearfix">
                              
                              <h3>Заказать дрова с доставкой</h3>
                             <div class="zig-zag-type1">
                                <span></span><span></span><span></span><span></span><span></span>
                                </div> 
                              
                              <div class="modal-good-container" id="modalGoodContainer">
                                <div class="modal-good-area clearfix">
                                    <div class="modal-good-img" id="modalGoodImg">                                      
                                    </div>
                                    <div class="modal-good-text">
                                        <div class="modal-good-name" id="modalGoodName"></div>
                                        <div class="good-card-price">
                                            <span id="modalGoodPrice"></span> 
                                            <i class="fa fa-rub" aria-hidden="true"></i>
                                        </div>
                                    </div>
                                </div>
                              </div>
                              
                              <div class="clearfix">
                                  <?= CHtml::beginForm([], 'post', ['class' => 'middle-form','id' => 'modalOrderForm'])?>    
                                                      <div class='hidden'>
                                                          <input type="text" name="title" id='modalOrderFormTitle' value="Дрова - Заказ из всплывающей формы" />
                                                      </div>
                                                  <div class="form-row">   
                                                      <div class="input-container">                                
                                                          <input type="tel" placeholder="Ваш телефон" name='phone' id='modalOrderClientPhone' required="required">
                                                      </div> 
                                                      <div class="input-container">
                                                          <div class='submit-container'>
                                                              <button id="headerFormSubmit" type="submit">Заказать дрова</button>
                                                          </div>
                                                      </div>
                                                  </div>                               
                                  </form> 
                                  </div>
                          </div>
                      </div>


                    </div>
 </div>

<div class="modal fade" tabindex="-1" role="dialog" aria-labelledby="succesModal" id='succesModal'>
                        
                    <div class="modal-dialog " role="document">
                      <div class="modal-content " >
                          <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                          
                          <div class="modal-video-form-container clearfix">
                              
                              <h3>Спасибо! <br> Ваш заказ принят.</h3>
                            
                          </div>
                      </div>


                    </div>
 </div>
