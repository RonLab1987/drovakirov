<section class="director-section">
    <div class="container">
        <div class="row">
            <div class=" col-xs-12 col-sm-11 col-md-8 col-lg-6 director-message-container">
                <h3>Директор компании</h3>
                <div class="director-message-text">
                    <p>
                        Здравствуйте, меня зовут Кумачёв Евгений и я являюсь директором компании «Дрова-Киров».
                    </p>
                    <p>
                        Более 8 лет я занимаюсь заготовкой и производством дров и мне нравится дарить людям тепло.
                    </p>
                    <p>
                        По любым вопросам связанным с качеством или несвоевременной доставкой дров, 
                        вы можете напрямую связаться со мной по <a href="tel:+78332771051">т. 77-10-51</a>  
                    </p>
                </div>
            </div>
        </div>    
    </div>
</section>    