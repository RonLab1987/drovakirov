<section class="price-section" id='price-section'>
    <div class="container">
        <h2>Цены и условия доставки</h2>
        
        <div class="row goods-area">
             <?php 
             for($i=0; $i<8; $i++){
                $this->renderPartial('_good_card'); 
             }
             ?>            
        </div>
        
        <div class="big-ax-area clearfix">
            <span class="big-ax-i-area">
                <span class="big-ax-i"><i>i</i></span>
            </span>
            <div class="big-ax-info">
                <p>Цена указана с доставкой по г. Кирову а/ми ЗиЛ-130, либо Газ-53.</p>
                <p>Доставка в районы считается по тарифу 16 руб/км в обе стороны.</p>
            </div>
        </div>
    </div>
    
</section>