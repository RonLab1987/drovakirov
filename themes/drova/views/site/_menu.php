<div class="menu-area hidden-xs hidden-sm">
    
    <div class="menu-container ">
        <div class="container">
                <ul class='nav roll-link landing-menu' id="menu" >
                    <li role="presentation" >
                        <a href="#priceSection" >Цены и условия доставки</a>
                    </li>  
                    <li role="presentation" >
                        <a href="#advSection">Почему мы</a>
                    </li>  
                    <li role="presentation" >
                        <a href="#ourProdaction">Наше производство</a>
                    </li>  
                    <li role="presentation" >
                        <a href="#videoSection">Видео о нас</a>
                    </li>  
                    <li role="presentation" >
                        <a href="#stepSection">Как оформить заказ</a>
                    </li>  
                    <li>
                        <a href="#reviewSection">Отзывы</a>
                    </li role="presentation" >  
                    <li>
                        <a href="#contactSection">Контакты</a>
                    </li>  
                </ul>

        </div>
    </div>
</div>