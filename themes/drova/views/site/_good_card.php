<div class="col-xs-12 col-sm-6 col-md-3 col-lg-3">
                <div class='good-card'>
                    <h3>Береза колотая</h3>
                    <div class="good-card-img-area">
                        <div class="good-card-img" style="background-image: url('img/bereza-kolotaya.jpg')">                            
                        </div>
                    </div>
                    <div class="good-card-price-area">
                        <div class="good-card-price">
                            5500 <i class="fa fa-rub" aria-hidden="true"></i>
                        </div>
                        <div class="good-card-bye-area">
                            <a href="" class="good-card-bye-btn">Заказать</a>
                        </div>
                    </div>
                </div>                
</div>