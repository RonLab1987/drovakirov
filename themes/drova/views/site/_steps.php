<section class="step-section" id="stepSection">
    <div class="container">
        <h3>Как оформить заказ?</h3>
        
        <div class="row">
            <div class="step-sub-row">
                <div class="col-xs-12 col-sm-6 ">
                    <div class="step-card" data-toggle="modal" data-target="#modalOrder">
                        <div class="step-card-icon-area">
                            <div class="step-card-icon">
                                 <img src="<?= $this->mainAssets ?>/img/step1-icon.png">
                            </div>
                            <div class="step-card-addon-icon">
                                <img src="<?= $this->mainAssets ?>/img/step-arrow-right.png">
                            </div>
                        </div>
                        <div class="step-card-text">
                            Вы звоните мастеру по т.77-10-51 или оставляете заявку на сайте.
                        </div>
                    </div>
                </div>
                <div class="col-xs-12 col-sm-6 ">
                    <div class="step-card" data-toggle="modal" data-target="#modalOrder">
                        <div class="step-card-icon-area">
                            <div class="step-card-icon">
                                 <img src="<?= $this->mainAssets ?>/img/step2-icon.png">
                            </div>
                            <div class="step-card-addon-icon">
                                <img src="<?= $this->mainAssets ?>/img/step-arrow-right.png">
                            </div>
                        </div>
                        <div class="step-card-text">
                            В течении 15 минут Наш мастер Евгений связывается с вами и уточняет какие дрова, когда и куда нужно привезти.
                        </div>
                    </div>
                </div>
            </div>
            
            <div class="step-sub-row">
                <div class="col-xs-12 col-sm-6 ">
                    <div class="step-card" data-toggle="modal" data-target="#modalOrder">
                        <div class="step-card-icon-area">
                            <div class="step-card-icon">
                                 <img src="<?= $this->mainAssets ?>/img/step3-icon.png">
                            </div>
                            <div class="step-card-addon-icon">
                                <img src="<?= $this->mainAssets ?>/img/step-arrow-right.png">
                            </div>
                        </div>
                        <div class="step-card-text">
                            Вы обо всём договариваетесь. Мы грузим и везём дрова к вам.
                        </div>
                    </div>
                </div>
                <div class="col-xs-12 col-sm-6 ">
                    <div class="step-card" data-toggle="modal" data-target="#modalOrder">
                        <div class="step-card-icon-area">
                            <div class="step-card-icon">
                                 <img src="<?= $this->mainAssets ?>/img/step4-icon.png">
                            </div>
                            <div class="step-card-addon-icon">
                            </div>
                        </div>
                        <div class="step-card-text">
                            Расчёт производите с водителем на месте. 
                        </div>
                    </div>
                </div>
            </div>
            
        </div>
        
    </div>    
</section>