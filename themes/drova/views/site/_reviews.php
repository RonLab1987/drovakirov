<section class="reviews-section">
    <div class="review-back-img">
        <div class="container">
            <h3>Люди о нас</h3>
            <div class="zig-zag-type1">
                <span></span><span></span><span></span><span></span><span></span>
            </div> 

            <div class="reviews-slider-container"> 
                <div id='reviews-slider' class="owl-carousel">
                    <div class="review-blanc-area">
                        <div class="review-blanc-top"></div>
                        <div class="review-blanc-container">
                            <img src="img/review/1.jpg" alt="" />
                        </div>
                        <div class="review-blanc-bottom"></div>
                    </div>
                    <div class="review-blanc-area">
                        <div class="review-blanc-top"></div>
                        <div class="review-blanc-container">
                            <img src="img/review/2.jpg" alt="" />
                        </div>
                        <div class="review-blanc-bottom"></div>
                    </div>
                    <div class="review-blanc-area">
                        <div class="review-blanc-top"></div>
                        <div class="review-blanc-container">
                            <img src="img/review/3.jpg" alt="" />
                        </div>
                        <div class="review-blanc-bottom"></div>
                    </div>      
                </div>
                <div class="reviews-slider-nav">
                    <div class="reviews-slider-nav-prev" id='reviewsPrev'></div>
                    <div class="reviews-slider-nav-next" id='reviewsNext'></div>
                </div> 
            </div>
        </div>
    </div>
</section>


<?php
    Yii::app()->getClientScript()->registerScriptFile('/js/review.ini.js', CClientScript::POS_END);
?>