<?php 
//$youtubeID = 'Mw1fDkg5ydA';
$youtubeID = 'Rurt8jspWrk';

Yii::app()->getClientScript()->registerScriptFile('https://www.youtube.com/iframe_api', CClientScript::POS_END);
Yii::app()->getClientScript()->registerScriptFile('/js/video.ini.js', CClientScript::POS_END);
?>
<script type="text/javascript">
        var youtubeID = '<?= $youtubeID;?>';
</script>

<section class="video-section" id="videoSection">
    <div class="container">
        <div class="row">
            <div class="video-section-addon"></div>
            <div class="col-xs-12 col-sm-11 col-md-10 col-lg-8 video-card-container">
                <div class="video-card" id='videoCard' data-toggle="modal" data-target="#modalVideoModal" style="background-image: url('http://img.youtube.com/vi/<?= $youtubeID; ?>/maxresdefault.jpg')">
                    <div class="video-card-content">    
                        <div class="zig-zag-type1">
                            <span></span><span></span><span></span><span></span><span></span>
                        </div> 
                        <h3>Видео о том, как мы работаем</h3>
                        <div class="video-play-area" >
                            <div class="video-play-btn">
                                <i class="fa fa-play-circle" aria-hidden="true"></i>
                            </div>
                            <div class="video-play-cta">
                                Посмотрите <br> видео
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        

<div class="modal fade" tabindex="-1" role="dialog" aria-labelledby="modalVideoModal" id='modalVideoModal'>
  <div class="modal-dialog modal-lg" role="document">
    <div class="modal-content" >
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                          
        <div id='modalVideoContainer'></div>
        <div class="modal-video-form-container clearfix">
            
            <h3>Заказать дрова с доставкой</h3>
            <div class="zig-zag-type1">
                        <span></span><span></span><span></span><span></span><span></span>
                    </div> 
            <div class="clearfix">
                <?= CHtml::beginForm([], 'post', ['class' => 'middle-form','id' => 'modalVideoForm'])?>    
                                    <div class='hidden'>
                                        <input type="text" name="title" value="Дрова - Заявка из видео формы" />
                                    </div>
                                <div class="form-row">   
                                    <div class="input-container">                                
                                        <input type="tel" placeholder="Ваш телефон" name='phone' id='clientPhoneVideoModal' required="required">
                                    </div> 
                                    <div class="input-container">
                                        <div class='submit-container'>
                                            <button id="headerFormSubmit" type="submit">Заказать дрова</button>
                                        </div>
                                    </div>
                                </div>                               
                </form> 
                </div>
        </div>
    </div>
        
    
  </div>
</div>
        
        
    </div>
</section>    